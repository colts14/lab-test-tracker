package com.example.slo_mo.labtesttracker.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by slo-mo on 12/5/2017.
 */

@Entity(tableName = "test")
public class Test {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "testId")
    private int testId;

    @ColumnInfo(name = "patientId")
    private int patientId;

    @ColumnInfo(name = "nurseId")
    private String nurseId;

    @ColumnInfo(name = "BPH")
    private int BPH;

    @ColumnInfo(name = "BPL")
    private int BPL;

    @ColumnInfo(name = "temperature")
    private float temperature;

    @ColumnInfo(name = "breathspermin")
    private int breathsPerMin;

    @ColumnInfo(name = "heartbeatspermin")
    private int heartbeatsPerMin;

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getNurseId() {
        return nurseId;
    }

    public void setNurseId(String nurseId) {
        this.nurseId = nurseId;
    }

    public int getBPH() {
        return BPH;
    }

    public void setBPH(int BPH) {
        this.BPH = BPH;
    }

    public int getBPL() {
        return BPL;
    }

    public void setBPL(int BPL) {
        this.BPL = BPL;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getBreathsPerMin() {
        return breathsPerMin;
    }

    public void setBreathsPerMin(int breathsPerMin) {
        this.breathsPerMin = breathsPerMin;
    }

    public int getHeartbeatsPerMin() {
        return heartbeatsPerMin;
    }

    public void setHeartbeatsPerMin(int heartbeatsPerMin) {
        this.heartbeatsPerMin = heartbeatsPerMin;
    }
}
