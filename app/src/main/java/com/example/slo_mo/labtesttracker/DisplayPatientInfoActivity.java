package com.example.slo_mo.labtesttracker;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Patient;

public class DisplayPatientInfoActivity extends AppCompatActivity {

    private int patientId;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_patient_info);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        setPatientId(getIntent().getExtras().getInt("PATIENT_ID"));
        new DisplayPatientInfoAsync().execute();

    }

    //---- Async task to display patient info ----
    private class DisplayPatientInfoAsync extends AsyncTask<Void, Void, Patient> {

        @Override
        protected Patient doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            Patient patient = myDb.db.patientDao().getPatientWithId(getPatientId());
            return patient;
        }

        @Override
        protected void onPostExecute(Patient patient) {
            super.onPostExecute(patient);
            if (patient == null) {
                TextView _tvNotFound = (TextView) findViewById(R.id.tvNotFound);
                _tvNotFound.setText("Patient not found.\nPlease check Patient Id");
            } else {
                // Create handles to views and set text
                EditText _txtpiPatientId = (EditText) findViewById(R.id.txtpiPatientId);
                _txtpiPatientId.setText(Integer.toString(patient.getPatientId()), TextView.BufferType.NORMAL);
                EditText _txtpiFirstName = (EditText) findViewById(R.id.txtpiFirstName);
                _txtpiFirstName.setText(patient.getFirstname(), TextView.BufferType.NORMAL);
                EditText _txtpiLastName = (EditText) findViewById(R.id.txtpiLastName);
                _txtpiLastName.setText(patient.getLastname(), TextView.BufferType.NORMAL);
                EditText _txtpiDoctorId = (EditText) findViewById(R.id.txtpiDoctorId);
                _txtpiDoctorId.setText(patient.getDoctorId(), TextView.BufferType.NORMAL);
                EditText _txtpiRoom = (EditText) findViewById(R.id.txtpiRoom);
                _txtpiRoom.setText(patient.getRoom(), TextView.BufferType.NORMAL);
                EditText _txtpiDept = (EditText) findViewById(R.id.txtpiDept);
                _txtpiDept.setText(patient.getDepartment(), TextView.BufferType.NORMAL);
            }
        }
    }

}
