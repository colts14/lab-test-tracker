package com.example.slo_mo.labtesttracker.database;

import android.arch.persistence.room.Room;

/**
 * Created by slo-mo on 11/25/2017.
 */

public class Singleton {
    private static Singleton ourInstance = null;

    public AppDatabase db;

    public static Singleton getInstance() {
        if (ourInstance == null)
            ourInstance = new Singleton();
        return ourInstance;
    }

    private Singleton() {
    }
}
