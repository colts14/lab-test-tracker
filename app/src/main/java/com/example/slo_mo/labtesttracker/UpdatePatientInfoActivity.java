package com.example.slo_mo.labtesttracker;

import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Patient;

public class UpdatePatientInfoActivity extends AppCompatActivity {
    private Spinner _spinner;
    private ArrayAdapter adapter;
    private int patientId;
    private String firstName;
    private String lastName;
    private String room;
    private String department;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_patient_info);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        _spinner = (Spinner) findViewById(R.id.spindept);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.department_array, R.layout.spinner_item);
        setupSpinner();

        setPatientId(getIntent().getExtras().getInt("PATIENT_ID"));
        new DisplayPatientInfoAsync().execute();

        final EditText _txtpiFirstName = (EditText) findViewById(R.id.txtpiFirstName);
        final EditText _txtpiLastName = (EditText) findViewById(R.id.txtpiLastName);
        final EditText _txtpiRoom = (EditText) findViewById(R.id.txtpiRoom);

        Button _btnpiUpdatePatientInfo = (Button) findViewById(R.id.btnUpdate);

        _btnpiUpdatePatientInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Return from method if any input validation fails
                if (_txtpiFirstName.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient first name is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtpiLastName.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient last name is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtpiRoom.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient room number is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (getDepartment().equals(getResources().getString(R.string.department_prompt))) {
                    Toast.makeText(getApplicationContext(), "Department is required", Toast.LENGTH_LONG).show();
                    return;
                }

                setFirstName(_txtpiFirstName.getText().toString());
                setLastName(_txtpiLastName.getText().toString());
                setRoom(_txtpiRoom.getText().toString());

                new UpdatePatientInfoAsync().execute();
            }
        });

    }

    //---- Async task to display patient info ----
    private class DisplayPatientInfoAsync extends AsyncTask<Void, Void, Patient> {

        @Override
        protected Patient doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            Patient patient = myDb.db.patientDao().getPatientWithId(getPatientId());
            return patient;
        }

        @Override
        protected void onPostExecute(Patient patient) {
            super.onPostExecute(patient);
            if (patient == null) {
                View _btn = findViewById(R.id.btnUpdate);
                _btn.setVisibility(View.GONE);
                TextView _tvNotFound = (TextView) findViewById(R.id.tvNotFound);
                _tvNotFound.setText("Patient not found.\nPlease check Patient Id");
            } else {
                // Create handles to views and set text
                EditText _txtpiPatientId = (EditText) findViewById(R.id.txtpiPatientId);
                _txtpiPatientId.setText(Integer.toString(patient.getPatientId()), TextView.BufferType.NORMAL);
                EditText _txtpiFirstName = (EditText) findViewById(R.id.txtpiFirstName);
                _txtpiFirstName.setText(patient.getFirstname(), TextView.BufferType.NORMAL);
                EditText _txtpiLastName = (EditText) findViewById(R.id.txtpiLastName);
                _txtpiLastName.setText(patient.getLastname(), TextView.BufferType.NORMAL);
                EditText _txtpiDoctorId = (EditText) findViewById(R.id.txtpiDoctorId);
                _txtpiDoctorId.setText(patient.getDoctorId(), TextView.BufferType.NORMAL);
                EditText _txtpiRoom = (EditText) findViewById(R.id.txtpiRoom);
                _txtpiRoom.setText(patient.getRoom(), TextView.BufferType.NORMAL);

                _spinner.setSelection(adapter.getPosition(patient.getDepartment()));
                setDepartment(patient.getDepartment());

            }
        }
    }

    //---- Async task to update patient info ----
    private class UpdatePatientInfoAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            Patient patient = myDb.db.patientDao().getPatientWithId(getPatientId());
            patient.setFirstname(getFirstName());
            patient.setLastname(getLastName());
            patient.setRoom(getRoom());
            patient.setDepartment(getDepartment());
            myDb.db.patientDao().update(patient);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(getApplicationContext(), "Database error", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Patient record updated", Toast.LENGTH_LONG).show();
        }
    }

    //---- Set up the spinner ----
    public void setupSpinner() {
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        _spinner.setAdapter(adapter);
        _spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setDepartment((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
