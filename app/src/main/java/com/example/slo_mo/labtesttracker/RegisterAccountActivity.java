package com.example.slo_mo.labtesttracker;

import android.app.Application;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Doctor;
import com.example.slo_mo.labtesttracker.models.Nurse;

public class RegisterAccountActivity extends AppCompatActivity {

    private String staff;
    private String department;
    private String userId;
    private String firstName;
    private String lastName;
    private String password;

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_account);

        //---- Hide AppBar for fullscreen ----
        ActionBar ab = getSupportActionBar();
        ab.hide();

        setStaff(getResources().getString(R.string.staff_prompt));
        setDepartment(getResources().getString(R.string.department_prompt));
        setupSpinners();

        final EditText _txtuid = (EditText) findViewById(R.id.txtuid_reg);
        final EditText _txtfname = (EditText) findViewById(R.id.txtfname_reg);
        final EditText _txtlname = (EditText) findViewById(R.id.txtlname_reg);
        final EditText _txtpass = (EditText) findViewById(R.id.txtpass_reg);
        Button _btnreg = (Button) findViewById(R.id.btn_reg);

        _btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---- Return from method if any input validation fails ----
                if (_txtuid.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "User Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtfname.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Firstname is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtlname.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Lastname is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtpass.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Password is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (getStaff().equals(getResources().getString(R.string.staff_prompt))) {
                    Toast.makeText(getApplicationContext(), "Healthcare staff is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (getDepartment().equals(getResources().getString(R.string.department_prompt))) {
                    Toast.makeText(getApplicationContext(), "Department is required", Toast.LENGTH_LONG).show();
                    return;
                }

                setUserId(_txtuid.getText().toString());
                setFirstName(_txtfname.getText().toString());
                setLastName(_txtlname.getText().toString());
                setPassword(_txtpass.getText().toString());

                new InsertAsync().execute();
            }
        });
    }

    private class InsertAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();

            Doctor doctor = myDb.db.doctorDao().getDoctorWithId(getUserId());
            Nurse nurse = myDb.db.nurseDao().getNurseWithId(getUserId());
            if ((doctor != null) || (nurse != null)) {
                cancel(true);
                return null;
            }

            if (getStaff().equals("Doctor")) {
                doctor = new Doctor();
                doctor.setDoctorId(getUserId());
                doctor.setFirstname(getFirstName());
                doctor.setLastname(getLastName());
                doctor.setPassword(getPassword());
                doctor.setDepartment(getDepartment());
                myDb.db.doctorDao().insert(doctor);
            } else {
                nurse = new Nurse();
                nurse.setNurseId(getUserId());
                nurse.setFirstname(getFirstName());
                nurse.setLastname(getLastName());
                nurse.setPassword(getPassword());
                nurse.setDepartment(getDepartment());
                myDb.db.nurseDao().insert(nurse);
            }
            return null;
        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            Toast.makeText(getApplicationContext(), getUserId() + " is already taken", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }

    //---- Set up the spinners ----
    public void setupSpinners() {
        Spinner _spinner1 = (Spinner) findViewById(R.id.spinner1_reg);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.staff_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        _spinner1.setAdapter(adapter);
        _spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setStaff((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        Spinner _spinner2 = (Spinner) findViewById(R.id.spinner2_reg);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.department_array, R.layout.spinner_item);
        adapter2.setDropDownViewResource(R.layout.spinner_dropdown_item);
        _spinner2.setAdapter(adapter2);
        _spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setDepartment((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }

}
