package com.example.slo_mo.labtesttracker.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.slo_mo.labtesttracker.models.Doctor;
import com.example.slo_mo.labtesttracker.models.Nurse;
import com.example.slo_mo.labtesttracker.models.Patient;
import com.example.slo_mo.labtesttracker.models.Test;

/**
 * Created by slo-mo on 11/25/2017.
 */

@Database(entities = {Doctor.class, Nurse.class, Patient.class, Test.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DoctorDao doctorDao();
    public abstract NurseDao nurseDao();
    public abstract PatientDao patientDao();
    public abstract TestDao testDao();
}
