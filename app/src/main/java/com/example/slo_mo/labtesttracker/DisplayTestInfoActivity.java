package com.example.slo_mo.labtesttracker;

import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Test;

public class DisplayTestInfoActivity extends AppCompatActivity {

    private int testId;

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_test_info);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        setTestId(getIntent().getExtras().getInt("TEST_ID"));
        new DisplayTestInfoAsync().execute();

    }

    //---- Async task to display patient test info ----
    private class DisplayTestInfoAsync extends AsyncTask<Void, Void, Test> {

        @Override
        protected Test doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            Test test = myDb.db.testDao().getTestWithId(getTestId());
            return test;
        }

        @Override
        protected void onPostExecute(Test test) {
            super.onPostExecute(test);
            if (test == null) {
                TextView _tvtiNotFound = (TextView) findViewById(R.id.tvtiNotFound);
                _tvtiNotFound.setText("Test not found.\nPlease check Test Id");
            } else {
                // Create handles to views and set text
                EditText _txttiTestId = (EditText) findViewById(R.id.txttiTestId);
                _txttiTestId.setText(Integer.toString(test.getTestId()), TextView.BufferType.NORMAL);
                EditText _txttiPatientId = (EditText) findViewById(R.id.txttiPatientId);
                _txttiPatientId.setText(Integer.toString(test.getPatientId()), TextView.BufferType.NORMAL);
                EditText _txttiNurseId = (EditText) findViewById(R.id.txttiNurseId);
                _txttiNurseId.setText(test.getNurseId(), TextView.BufferType.NORMAL);
                EditText _txttiBPH = (EditText) findViewById(R.id.txttiBPH);
                _txttiBPH.setText(Integer.toString(test.getBPH()), TextView.BufferType.NORMAL);
                EditText _txttiBPL = (EditText) findViewById(R.id.txttiBPL);
                _txttiBPL.setText(Integer.toString(test.getBPL()), TextView.BufferType.NORMAL);
                EditText _txttiTemperature = (EditText) findViewById(R.id.txttiTemperature);
                _txttiTemperature.setText(Float.toString(test.getTemperature()), TextView.BufferType.NORMAL);
                EditText _txttiBreathsPerMin = (EditText) findViewById(R.id.txttiBreathsPerMin);
                _txttiBreathsPerMin.setText(Integer.toString(test.getBreathsPerMin()), TextView.BufferType.NORMAL);
                EditText _txttiHeartbeatsPerMin = (EditText) findViewById(R.id.txttiHeartbeatsPerMin);
                _txttiHeartbeatsPerMin.setText(Integer.toString(test.getHeartbeatsPerMin()), TextView.BufferType.NORMAL);

            }
        }
    }
}
