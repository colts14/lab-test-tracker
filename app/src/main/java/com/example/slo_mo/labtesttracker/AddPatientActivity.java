package com.example.slo_mo.labtesttracker;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Patient;

import org.w3c.dom.Text;

public class AddPatientActivity extends AppCompatActivity {

    private String firstName;
    private String lastName;
    private String room;
    private String doctorId;
    private String department;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        setDepartment(getResources().getString(R.string.department_prompt));
        setupSpinner();

        final EditText _txtpfname = (EditText) findViewById(R.id.txtpfname);
        final EditText _txtplname = (EditText) findViewById(R.id.txtplname);
        final EditText _txtroom = (EditText) findViewById(R.id.txtroom);
        final EditText _txtdoctorid = (EditText) findViewById(R.id.txtdoctorid);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("SharedPref", MODE_PRIVATE);
        setDoctorId(pref.getString("userid", null));
        _txtdoctorid.setText(getDoctorId(), TextView.BufferType.NORMAL);
        _txtdoctorid.setEnabled(false);
        Button _btnAddPatient = (Button) findViewById(R.id.btnaddpatient);

        _btnAddPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Return from method if any input validation fails
                if (_txtpfname.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient first name is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtplname.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient last name is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (getDepartment().equals(getResources().getString(R.string.department_prompt))) {
                    Toast.makeText(getApplicationContext(), "Department is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtroom.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Room number is required", Toast.LENGTH_LONG).show();
                    return;
                }

                setFirstName(_txtpfname.getText().toString());
                setLastName(_txtplname.getText().toString());
                setRoom(_txtroom.getText().toString());

                new InsertAsync().execute();

            }
        });
    }

    //---- Async task to insert patient ----
    private class InsertAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            Patient p = new Patient();
            p.setFirstname(getFirstName());
            p.setLastname(getLastName());
            p.setDoctorId(getDoctorId());
            p.setDepartment(getDepartment());
            p.setRoom(getRoom());
            myDb.db.patientDao().insert(p);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(getApplicationContext(), "Database error", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Patient record added", Toast.LENGTH_LONG).show();
        }
    }


    //---- Set up the spinner ----
    public void setupSpinner() {
        Spinner _spinner = (Spinner) findViewById(R.id.spindept);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.department_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        _spinner.setAdapter(adapter);
        _spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setDepartment((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
