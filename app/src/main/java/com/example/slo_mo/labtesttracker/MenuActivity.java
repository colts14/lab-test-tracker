package com.example.slo_mo.labtesttracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        final EditText _txtPatientId = (EditText) findViewById(R.id.txtPatientId);
        final EditText _txtTestId = (EditText) findViewById(R.id.txtTestId);
        Button _btnDisplayPatients = (Button) findViewById(R.id.btnDisplayPatients);
        Button _btnDisplayPatient = (Button) findViewById(R.id.btnDisplayPatient);
        Button _btnAddPatient = (Button) findViewById(R.id.btnAddPatient);
        Button _btnUpdatePatient = (Button) findViewById(R.id.btnUpdatePatient);
        Button _btnAddTest = (Button) findViewById(R.id.btnAddTest);
        Button _btnDisplayTests = (Button) findViewById(R.id.btnDisplayTests);
        Button _btnDisplayTestData = (Button) findViewById(R.id.btnDisplayTestData);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("SharedPref", MODE_PRIVATE);

        _btnDisplayPatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, DisplayPatientsActivity.class);
                startActivity(intent);
            }
        });

        _btnDisplayPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---- Return from method if any input validation fails ----
                if (_txtPatientId.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MenuActivity.this, DisplayPatientInfoActivity.class);
                intent.putExtra("PATIENT_ID", Integer.parseInt(_txtPatientId.getText().toString()));
                startActivity(intent);
            }
        });

        _btnAddPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Objects.equals(pref.getString("staff", null), "Doctor")){
                    Toast.makeText(getApplicationContext(), "Only doctors can add patients", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MenuActivity.this, AddPatientActivity.class);
                startActivity(intent);
            }
        });

        _btnUpdatePatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---- Return from method if any input validation fails ----
                if (_txtPatientId.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MenuActivity.this, UpdatePatientInfoActivity.class);
                intent.putExtra("PATIENT_ID", Integer.parseInt(_txtPatientId.getText().toString()));
                startActivity(intent);
            }
        });

        _btnAddTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---- Return from method if any input validation fails ----
                if (_txtPatientId.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!Objects.equals(pref.getString("staff", null), "Nurse")){
                    Toast.makeText(getApplicationContext(), "Only nurses can add test data", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MenuActivity.this, EnterTestDataActivity.class);
                intent.putExtra("PATIENT_ID", Integer.parseInt(_txtPatientId.getText().toString()));
                startActivity(intent);
            }
        });

        _btnDisplayTests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---- Return from method if any input validation fails ----
                if (_txtPatientId.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Patient Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MenuActivity.this, DisplayTestsActivity.class);
                intent.putExtra("PATIENT_ID", Integer.parseInt(_txtPatientId.getText().toString()));
                startActivity(intent);
            }
        });

        _btnDisplayTestData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---- Return from method if any input validation fails ----
                if (_txtTestId.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Test Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MenuActivity.this, DisplayTestInfoActivity.class);
                intent.putExtra("TEST_ID", Integer.parseInt(_txtTestId.getText().toString()));
                startActivity(intent);
            }
        });
    }
}
