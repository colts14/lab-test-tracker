package com.example.slo_mo.labtesttracker.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.slo_mo.labtesttracker.models.Patient;

import java.util.List;

/**
 * Created by slo-mo on 11/26/2017.
 */

@Dao
public interface PatientDao {
    // Adds a patient to the database
    @Insert
    void insert(Patient patient);

    // Removes a patient from the database
    @Delete
    void delete(Patient patient);

    // Updates patient
    @Update
    void update(Patient patient);

    // Gets all patients in the patient's table
    @Query("SELECT * FROM patient")
    List<Patient> getAllPatients();

    // Gets patient in the patient's table with particular id
    @Query("SELECT * FROM patient WHERE patientId LIKE :pId")
    Patient getPatientWithId(int pId);

}
