package com.example.slo_mo.labtesttracker.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.slo_mo.labtesttracker.models.Nurse;

import java.util.List;

/**
 * Created by slo-mo on 11/25/2017.
 */

@Dao
public interface NurseDao {

    // Adds a nurse to the database
    @Insert
    void insert(Nurse nurse);

    // Removes a nurse from the database
    @Delete
    void delete(Nurse nurse);

    // Gets all nurses in the nurse's table
    @Query("SELECT * FROM nurse")
    List<Nurse> getAllNurses();

    // Gets nurse in the nurse's table with particular id
    @Query("SELECT * FROM nurse WHERE nurseId LIKE :rnId")
    Nurse getNurseWithId(String rnId);

    // Gets nurse in the nurse's table with particular credentials
    @Query("SELECT * FROM nurse WHERE nurseId LIKE :rnId AND password LIKE :rnPwd")
    Nurse getNurseWithCreds(String rnId, String rnPwd);

}
