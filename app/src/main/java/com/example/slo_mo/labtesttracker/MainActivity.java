package com.example.slo_mo.labtesttracker;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.slo_mo.labtesttracker.database.AppDatabase;
import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Doctor;
import com.example.slo_mo.labtesttracker.models.Nurse;

public class MainActivity extends AppCompatActivity {
    private String staff;
    private String userId;
    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //---- Create single instance of RoomDatabase ----
        Singleton myDb = Singleton.getInstance();
        myDb.db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "labtest-database").build();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hide AppBar for fullscreen
        ActionBar ab = getSupportActionBar();
        ab.hide();

        setStaff(getResources().getString(R.string.staff_prompt));
        setupSpinner();

        final EditText _txtuid = (EditText) findViewById(R.id.txtuid);
        final EditText _txtpass = (EditText) findViewById(R.id.txtpass);
        Button _btnlogin = (Button) findViewById(R.id.btnlogin);
        TextView _btnreg = (TextView) findViewById(R.id.btnreg);

        _btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Return from method if any input validation fails
                if (_txtuid.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "User Id is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtpass.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Password is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (getStaff().equals(getResources().getString(R.string.staff_prompt))) {
                    Toast.makeText(getApplicationContext(), "Healthcare staff is required", Toast.LENGTH_LONG).show();
                    return;
                }

                setUserId(_txtuid.getText().toString());
                setPassword(_txtpass.getText().toString());

                new ValidateAsync().execute();

            }
        });


        // Intent For Opening RegisterAccountActivity
        _btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, RegisterAccountActivity.class);
                startActivity(intent);
            }
        });
    }

    //---- Async task to validate user credentials ----
    private class ValidateAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
                Singleton myDb2 = Singleton.getInstance();
                if (getStaff().equals("Doctor")) {
                    Doctor doctor = myDb2.db.doctorDao().getDoctorWithCreds(getUserId(), getPassword());
                    if (doctor == null) {
                        cancel(true);
                    }
                } else {
                    Nurse nurse = myDb2.db.nurseDao().getNurseWithCreds(getUserId(), getPassword());
                    if (nurse == null) {
                        cancel(true);
                    }
                }
            return null;
        }

        // Write user id into shared preferences
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            SharedPreferences prefs = getApplicationContext().getSharedPreferences("SharedPref", MODE_PRIVATE);
            Editor editor = prefs.edit();
            editor.putString("userid", getUserId());
            editor.putString("staff", getStaff());
            editor.commit();

            // Login successful, present menu
            Intent intent = new Intent(MainActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            Toast.makeText(getApplicationContext(), "User id / password / staff combination does not exist", Toast.LENGTH_LONG).show();
        }
    }

    //---- Set up the spinner ----
    public void setupSpinner() {
        Spinner _spinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.staff_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        _spinner.setAdapter(adapter);
        _spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setStaff((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

}
