package com.example.slo_mo.labtesttracker;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Test;

import java.util.List;

public class DisplayTestsActivity extends AppCompatActivity {
    private TextView _tvDisplayPatientTests;
    private int patientId;
    private String nurseId;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getNurseId() {
        return nurseId;
    }

    public void setNurseId(String nurseId) {
        this.nurseId = nurseId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_tests);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        _tvDisplayPatientTests = (TextView) findViewById(R.id.tvDisplayPatientTests);
        _tvDisplayPatientTests.setMovementMethod(new ScrollingMovementMethod());

        setPatientId(getIntent().getExtras().getInt("PATIENT_ID"));
        SharedPreferences pref = getApplicationContext().getSharedPreferences("SharedPref", MODE_PRIVATE);
        setNurseId(pref.getString("userid", null));

        new ShowPatientTestsAsync().execute();

    }

    //---- Async task to show patient's tests ----
    private class ShowPatientTestsAsync extends AsyncTask<Void, Void, List<Test>> {

        @Override
        protected List<Test> doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            List<Test> tests = myDb.db.testDao().getAllTestsWithPatientId(getPatientId());
            return tests;
        }

        @Override
        protected void onPostExecute(List<Test> tests) {
            super.onPostExecute(tests);
            String output = "";
            for (Test t : tests) {
                output += "Test Id: " + Integer.toString(t.getTestId()) + "\n";
                output += "Patient Id: " + Integer.toString(t.getPatientId()) + "\n";
                output += "Nurse Id: " + t.getNurseId() + "\n";
                output += "Systolic Pressure: " + Integer.toString(t.getBPH()) + "\n";
                output += "Diastolic Pressure: " + Integer.toString(t.getBPL()) + "\n";
                output += "Temperature: " + Float.toString(t.getTemperature()) + "\n";
                output += "Respiration: " + Integer.toString(t.getBreathsPerMin()) + "\n";
                output += "Heartrate: " + Integer.toString(t.getHeartbeatsPerMin()) + "\n\n";

            }
            _tvDisplayPatientTests.setText(output);
        }
    }
}
