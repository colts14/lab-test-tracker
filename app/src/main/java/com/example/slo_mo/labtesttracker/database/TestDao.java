package com.example.slo_mo.labtesttracker.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.slo_mo.labtesttracker.models.Test;

import java.util.List;

/**
 * Created by slo-mo on 12/5/2017.
 */

@Dao
public interface TestDao {
    // Adds a test to the database
    @Insert
    void insert(Test test);

    // Removes a test from the database
    @Delete
    void delete(Test test);

    // Updates test
    @Update
    void update(Test test);

    // Gets all tests in the test's table
    @Query("SELECT * FROM test")
    List<Test> getAllTests();

    // Gets all tests in the test's table with particular patient id
    @Query("SELECT * FROM test WHERE patientId LIKE :pId")
    List<Test> getAllTestsWithPatientId(int pId);

    // Gets test in the test's table with particular test id
    @Query("SELECT * FROM test WHERE testId LIKE :tId")
    Test getTestWithId(int tId);
}
