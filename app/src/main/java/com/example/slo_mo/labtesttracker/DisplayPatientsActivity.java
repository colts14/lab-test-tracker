package com.example.slo_mo.labtesttracker;

import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Patient;

import java.util.List;

public class DisplayPatientsActivity extends AppCompatActivity {
    private TextView _tvDisplayPatients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_patients);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        _tvDisplayPatients = (TextView) findViewById(R.id.tvDisplayPatients);
        _tvDisplayPatients.setMovementMethod(new ScrollingMovementMethod());
        new ShowPatientsAsync().execute();
    }

    //---- Async task to show patients ----
    private class ShowPatientsAsync extends AsyncTask<Void, Void, List<Patient>> {

        @Override
        protected List<Patient> doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            List<Patient> patients = myDb.db.patientDao().getAllPatients();
            return patients;
        }

        @Override
        protected void onPostExecute(List<Patient> patients) {
            super.onPostExecute(patients);
            String output = "";
            for (Patient p : patients) {
                output+= "Patient Id: " + p.getPatientId() + "\n";
                output+= "Firstname: " + p.getFirstname() + "\n";
                output+= "Lastname: " + p.getLastname() + "\n";
                output+= "Department: " + p.getDepartment() + "\n";
                output+= "Room: " + p.getRoom() + "\n\n";
            }
            _tvDisplayPatients.setText(output);
        }
    }
}
