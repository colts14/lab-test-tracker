package com.example.slo_mo.labtesttracker.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.slo_mo.labtesttracker.models.Doctor;

import java.util.List;

/**
 * Created by slo-mo on 11/25/2017.
 */

@Dao
public interface DoctorDao {

    // Adds a doctor to the database
    @Insert
    void insert(Doctor doctor);

    // Removes a doctor from the database
    @Delete
    void delete(Doctor doctor);

    // Gets all doctors in the doctor's table
    @Query("SELECT * FROM doctor")
    List<Doctor> getAllDoctors();

    // Gets doctor in the doctor's table with particular id
    @Query("SELECT * FROM doctor WHERE doctorId LIKE :drId")
    Doctor getDoctorWithId(String drId);

    // Gets doctor in the doctor's table with particular credentials
    @Query("SELECT * FROM doctor WHERE doctorId LIKE :drId AND password LIKE :drPwd")
    Doctor getDoctorWithCreds(String drId, String drPwd);
}
