package com.example.slo_mo.labtesttracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.slo_mo.labtesttracker.database.Singleton;
import com.example.slo_mo.labtesttracker.models.Test;

public class EnterTestDataActivity extends AppCompatActivity {

    private int patientId;
    private String nurseId;
    private int BPH;
    private int BPL;
    private float temperature;
    private int breathsPerMin;
    private int heartbeatsPerMin;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getNurseId() {
        return nurseId;
    }

    public void setNurseId(String nurseId) {
        this.nurseId = nurseId;
    }

    public int getBPH() {
        return BPH;
    }

    public void setBPH(int BPH) {
        this.BPH = BPH;
    }

    public int getBPL() {
        return BPL;
    }

    public void setBPL(int BPL) {
        this.BPL = BPL;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getBreathsPerMin() {
        return breathsPerMin;
    }

    public void setBreathsPerMin(int breathsPerMin) {
        this.breathsPerMin = breathsPerMin;
    }

    public int getHeartbeatsPerMin() {
        return heartbeatsPerMin;
    }

    public void setHeartbeatsPerMin(int heartbeatsPerMin) {
        this.heartbeatsPerMin = heartbeatsPerMin;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_test_data);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        final EditText _txtetdPatientId = (EditText) findViewById(R.id.txtetdPatientId);
        final EditText _txtetdNurseId = (EditText) findViewById(R.id.txtetdNurseId);
        final EditText _txtetdBPH = (EditText) findViewById(R.id.txtetdBPH);
        final EditText _txtetdBPL = (EditText) findViewById(R.id.txtetdBPL);
        final EditText _txtetdTemperature = (EditText) findViewById(R.id.txtetdTemperature);
        final EditText _txtetdBreathsPerMin = (EditText) findViewById(R.id.txtetdBreathsPerMin);
        final EditText _txtetdHeartbeatsPerMin = (EditText) findViewById(R.id.txtetdHeartBeatsPerMin);

        setPatientId(getIntent().getExtras().getInt("PATIENT_ID"));
        _txtetdPatientId.setText(Integer.toString(getPatientId()));
        _txtetdPatientId.setEnabled(false);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("SharedPref", MODE_PRIVATE);
        setNurseId(pref.getString("userid", null));
        _txtetdNurseId.setText(getNurseId(), TextView.BufferType.NORMAL);
        _txtetdNurseId.setEnabled(false);
        Button _btnetdSubmitData = (Button) findViewById(R.id.btnetdSubmitData);

        _btnetdSubmitData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Return from method if any input validation fails
                if (_txtetdBPH.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Systolic blood pressure is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtetdBPL.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Diastolic blood pressure is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtetdTemperature.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Temperature is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtetdBreathsPerMin.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Respiration is required", Toast.LENGTH_LONG).show();
                    return;
                }
                if (_txtetdHeartbeatsPerMin.getText().toString().trim().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Heartbeats per minute is required", Toast.LENGTH_LONG).show();
                    return;
                }

                setBPH(Integer.parseInt(_txtetdBPH.getText().toString()));
                setBPL(Integer.parseInt(_txtetdBPL.getText().toString()));
                setTemperature(Float.parseFloat(_txtetdTemperature.getText().toString()));
                setBreathsPerMin(Integer.parseInt(_txtetdBreathsPerMin.getText().toString()));
                setHeartbeatsPerMin(Integer.parseInt(_txtetdHeartbeatsPerMin.getText().toString()));

                new InsertAsync().execute();

            }
        });
    }

    //---- Async task to insert test data ----
    private class InsertAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Singleton myDb = Singleton.getInstance();
            Test t = new Test();
            t.setPatientId(getPatientId());
            t.setNurseId(getNurseId());
            t.setBPH(getBPH());
            t.setBPL(getBPL());
            t.setTemperature(getTemperature());
            t.setBreathsPerMin(getBreathsPerMin());
            t.setHeartbeatsPerMin(getHeartbeatsPerMin());

            myDb.db.testDao().insert(t);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(getApplicationContext(), "Database error", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Test record added", Toast.LENGTH_LONG).show();
        }
    }

}
